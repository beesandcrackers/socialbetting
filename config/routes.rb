Rails.application.routes.draw do
    # Set the new view of the sessions controller as the root location
    root 'sessions#new'

    # Handle callbacks from OmniAuth by passing information to the create view
    match '/auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
    match '/auth/failure', to: 'sessions#failure', via: [:get, :post]

    # Handle logout by destroying the current session
    match '/logout', to: 'sessions#destroy', :as => 'logout', via: [:get, :post]

    # Add views to Rails' routes
    get 'sessions/index'
    get 'sessions/create'

    # Set identities and wagers as resources to streamline routing
    resources :identities
    resources :wagers
end
