class User < ActiveRecord::Base
    # Treat an array of Facebook friends as a single entity to allow
    # database storage
    serialize :friends, Array

    # Either find a User with the provided information, or pass the data
    # to create_with_omniauth
    def self.from_omniauth(auth)
        find_by_provider_and_uid(auth['provider'], auth['uid']) || create_with_omniauth(auth)
    end

    # Create a new User with the provided credentials
    # The flow control is due to the fact that Venmo's JSON output is
    # formatted very differently from Facebook's and RunKeeper's
    def self.create_with_omniauth(auth)
        require 'json'

        create! do |user|
            user.provider = auth['provider']
            user.uid = auth['uid']
            if auth['info']['name']
                user.name = auth['info']['name']
            else
                user.name = auth['extra']['raw_info']['display_name']
            end
        end
    end
end
