class ApplicationController < ActionController::Base
    # Prevent CSRF attacks by raising an exception.
    # For APIs, you may want to use :null_session instead.
    protect_from_forgery with: :exception

    # Create an application-wide helper method. (i.e. it allows developers
    # to call the current user's information with a single "object")
    helper_method :current_user

    protected

    # Get information about the current user, assuming the user has 
    # previously existed (it should never get here, otherwise)
    def current_user
        @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end
end
