class WagersController < ApplicationController
    # Method ot calculate time left
    def time_left(seconds)
        days = (seconds / 1.day).floor
        seconds -= days.days
        hours = (seconds / 1.hour).floor
        seconds -= hours.hours
        minutes = (seconds / 1.minute).floor
        seconds -= minutes.minutes
        { days: days, hours: hours, minutes: minutes, seconds: seconds }
    end

    # Method to gather information about current wagers 
    def index
        @wagers = Wager.where(:challenger => current_user.name)
        @wagers += Wager.where(:acceptor => current_user.name)
        @wagers.each do |wager|
            timeleft = self.time_left(Time.parse(wager.duration.to_s) - Time.parse(Time.now.to_s))
            wager.timeleft = timeleft[:days].to_s + " days, " +
                             timeleft[:hours].to_s + " hours, " +
                             timeleft[:minutes].to_s + " minutes, " +
                             timeleft[:seconds].to_s + " seconds left"
        end
    end

    # Helper method for create (is necessary to generate a form
    # to create a new wager.
    def new
        @wager = Wager.new
    end

    # Method to create a wager and then redirect the user to the
    # wager index.
    def create
        @wager = Wager.new(wager_params)
        @wager.challenger = current_user.name
        @wager.temp = rand(50)
        @wager.save

        redirect_to action: 'index'
    end

    # Method to access information from RunKeeper and display it for one
    # particular wager.
    def show
        require 'net/http'
        require 'uri'
        require 'json'

        # Determine type of activity (currently limited to 3 for beta purposes)
        @wager = Wager.find(params[:id])
        if @wager.activity == "Running"
            type = "Running"
        elsif @wager.activity == "Cycling"
            type = "Cycling"
        else
            type = "Swimming"
        end

        # Send an HTTP GET request to the server and receive the requested data
        uri = URI.parse("https://api.runkeeper.com/fitnessActivities/")

        # Do loop to handle HTTPS communication. Also, limit the scope of the requested data 
        Net::HTTP.start(uri.host, uri.port, :use_ssl => true, uri.scheme => 'https') do |http|
            request = Net::HTTP::Get.new(uri.request_uri)
            request["Authorization"] = session[:access_token]
            request["Accept"] = "application/vnd.com.runkeeper.User+json"
            params = {"noEarlierThan" => @wager.created_at.strftime("%Y%m%d"), "noLaterThan" => @wager.duration.strftime("%Y%m%d"),
                      "type" => type}
            uri.query = URI.encode_www_form(params)

            http = Net::HTTP.new(uri.host, uri.port)
            response = http.request(request)
        end

        @total_distance = response['total_distance']
    end

    def destroy
        @wager = Wager.find(params[:id])
        @wager.destroy

        redirect_to wagers_path
    end

    private
        # Private helper method to handle Rails 4's implementation of strong parameters
        def wager_params
            accessible = [:challenger, :acceptor, :amount, :activity, :duration, :created_at, :updated_at]
            params.require(:wager).permit(accessible)
        end
end
