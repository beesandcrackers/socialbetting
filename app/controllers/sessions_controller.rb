class SessionsController < ApplicationController
    # Global variable to keep track of the session's access token (necessary
    # to call APIs functions
    @@access_token = nil

    # Method to redirect the user based on whether or not they are new
    # to the application
    def new
        if current_user
            if @@access_token == nil
                redirect_to logout_path and return
            end

            redirect_to action: 'index'
        end
    end

    # Method to keep track of user's friends and two out of three types
    # of access tokens (Facebook and RunKeeper). Again, Venmo is the odd
    # one out because the structure of their API is so vastly different.
    def index
        # Checks to see if the current provider (basically, the service
        # that logged the user in) is Facebook. If so, it takes the 
        # access token and, from it, gathers a list of the current user's
        # Facebook friends who have used the application.
        if current_user.provider == "facebook"
            @graph = Koala::Facebook::API.new(@@access_token)
            profile = @graph.get_object("me")
            @friends = @graph.get_connections("me", "friends")
        # Check if the provider is Venmo, and return either an existing
        # friends list or nil
        elsif current_user.provider == "venmo"
            @friends = @friends != nil ? @friends : []
        # Finish the handshake with RunKeeper's API (which was started with
        # omniauth-runkeeper. Send's an HTTP POST request to the server,
        # which returns an access token.
        else
            if params[:code]
                @code = params[:code]
                
                require 'net/http'
                require 'uri'
                require 'json'

                uri = URI.parse("https://runkeeper.com/apps/token")
                response = Net::HTTP.post_form(uri, {"grant_type" => "authorization_code", "code" => @code, "client_id" => "9f2b9ca41fc844e3a68127e3116fb451", 
                                               "client_secret" => 'e4248f5bfafe49e3b6e66ff90c8ea885', "redirect_uri" => "loalhost:3000/auth/runkeeper/callback"})
                
                @@access_token = JSON.parse(response.body)['access_token']

                @friends = @friends != nil ? @friends : []
            end
        end

        # Converts hashes of information into a useable list of strings
        # (the names of the friends)
        friend_list = []
        if @friends
            @friends.each do |friend|
                if friend.key?('name')
                    friend_list.push(friend['name'])
                end
            end
        end
        current_user.friends = friend_list
        current_user.save
        return @friends
    end

    # Method to sign in users who already exist in the system. Also generates
    # access tokens.
    def create
        user = User.from_omniauth(env['omniauth.auth'])
        session[:user_id] = user.id
        @@access_token = request.env['omniauth.auth']['credentials']['token']
        session[:access_token] = @@access_token
        redirect_to root_url, notice: "Signed in!"
    end

    # Method to destroy the current session
    def destroy
        session[:user_id] = nil
        redirect_to root_url, notice: "Signed out!"
    end

    # Method which reports when authentication with a provider fails
    def failure
        redirect_to root_url, alert: "Authentication failed, please try again."
    end
end
