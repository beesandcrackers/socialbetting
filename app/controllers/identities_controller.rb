class IdentitiesController < ApplicationController
    # Method to create a new Identity via omniauth-identity
    def new
        @identity = env['omniauth.identity']
    end
end
