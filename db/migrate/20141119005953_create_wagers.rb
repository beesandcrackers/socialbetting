class CreateWagers < ActiveRecord::Migration
  def change
    create_table :wagers do |t|
      t.string :challenger
      t.string :acceptor
      t.decimal :amount
      t.string :activity
      t.datetime :duration

      t.timestamps
    end
  end
end
