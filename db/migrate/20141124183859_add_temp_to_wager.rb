class AddTempToWager < ActiveRecord::Migration
  def change
    add_column :wagers, :temp, :integer
  end
end
