class AddTimeleftToWager < ActiveRecord::Migration
  def change
    add_column :wagers, :timeleft, :datetime
  end
end
