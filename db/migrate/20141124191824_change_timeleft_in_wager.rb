class ChangeTimeleftInWager < ActiveRecord::Migration
    def up
        change_column :wagers, :timeleft, :string
    end

    def down
        change_column :wagers, :timeleft, :datetime
    end
end
